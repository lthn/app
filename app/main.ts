import {app, BrowserWindow, screen} from 'electron';
import * as path from 'path';
import * as fs from 'fs';
import * as url from 'url';
import {ConfigIniParser} from 'config-ini-parser';
// Main process

var pem = require('pem');
var os = require('os');

let win: BrowserWindow = null;
let lthn = null;
const args = process.argv.slice(1),
	serve = args.some(val => val === '--serve');

const letheanPaths = {
	app: app.getAppPath(),
	home: app.getPath('home'),
	conf: path.join(app.getPath('home'), 'Lethean', 'conf'),
	bin: path.join(app.getPath('home'), 'Lethean', 'bin'),
	data: path.join(app.getPath('home'), 'Lethean', 'data'),
	wallets: path.join(app.getPath('home'), 'Lethean', 'wallet')
};

let letheanConfig: ConfigIniParser;

/**
 * Checks for the existence of users, if there are no users, we dont fire up the daemon
 * and make some window adjustments.
 *
 * @returns boolean
 */
function firstRunCheck(): boolean {
	return fs.readdirSync(path.join(letheanPaths.home, 'Lethean', 'users')).filter(function (x) {
		return x.startsWith('.', 0) === false;
	}).length < 1;
}


function createWindow(): BrowserWindow {

	const electronScreen = screen;
	const size = electronScreen.getPrimaryDisplay().workAreaSize;

	let file = 'index.html';
	let windowWidth = size.width;
	let windowHeight = size.height;
	let windowPosX = 0;
	let windowPosY = 0;

	/**
	 * If this is the first run, change template to a lite template
	 */
	if (firstRunCheck()) {
		windowWidth = 1096;
		windowHeight = 738;
		windowPosX = size.width / 2;
		windowPosY = size.height / 2;
	}

	// Create the browser window.
	win = new BrowserWindow({
		x: windowPosX,
		y: windowPosY,
		width: windowWidth,
		height: windowHeight,
		webPreferences: {
			preload: path.join(__dirname, 'preload.js'),
			nodeIntegration: true,
			nodeIntegrationInWorker: true,
			allowRunningInsecureContent: true,
			contextIsolation: false,  // false if you want to run e2e test with Spectron
			webSecurity: false // true if you want to run e2e test with Spectron or use remote module in renderer context (ie. Angular)
		}
	});


	if (serve) {
		win.webContents.openDevTools();
		require('electron-reload')(__dirname, {
			electron: require(path.join(__dirname, '/../node_modules/electron'))
		});

	} else {
		// Path when running electron executable
		let pathIndex = `./${file}`;

		if (fs.existsSync(path.join(__dirname, `../dist/lethean/browser/${file}`))) {
			// Path when running electron in local folder
			pathIndex = `../dist/lethean/browser/${file}`;

		}

		win.loadURL(url.format({
				pathname: path.join(__dirname, pathIndex),
				protocol: 'file:',
				slashes: true
			})
		);
	}


// Emitted when the window is closed.
	win.on('closed', () => {
		// Dereference the window object, usually you would store window
		// in an array if your app supports multi windows, this is the time
		// when you should delete the corresponding element.
		win = null;
	});

	return win;
}

try {

	// This method will be called when Electron has finished
	// initialization and is ready to create browser windows.
	// Some APIs can only be used after this event occurs.
	// Added 400 ms to fix the black background issue while using transparent window. More detais at https://github.com/electron/electron/issues/15947
	app.on('ready', () => {
			setTimeout(createWindow, 400);
			/**
			 * this loads the app.ini file in for the application side
			 */
			if (!fs.readFileSync(path.join(letheanPaths.conf, 'private.pem'))) {

				if (!fs.existsSync(letheanPaths.conf)) {
					// Path when running electron in local folder
					fs.mkdirSync(letheanPaths.conf);
				}
				pem.createCertificate({days: 3000, selfSigned: true}, function (err, keys) {
					if (err) {
						throw err;
					}
					fs.writeFileSync(path.join(letheanPaths.conf, 'private.pem'), keys.serviceKey);
					fs.writeFileSync(path.join(letheanPaths.conf, 'public.pem'), keys.certificate);

				});
			}

			if (fs.existsSync(path.join(__dirname, '../dist/lethean/browser/index.html'))) {
				// Path when running electron in local folder
				letheanPaths.app = letheanPaths.app + '/app';
			}
			const spawn = require('child_process').spawn;
			const executablePath = path.join(letheanPaths.app, 'cli', 'lthn' + (os.platform() === 'win32' ? '.exe' : ''));

			//console.error(executablePath, parameters);
			lthn = spawn(executablePath, ['backend', 'start'], {}).on('error', function (err) {
				throw err;
			}).stdout.on('data', (data) => {
				if (win !== undefined) {
					win.webContents.send('lthn-stdout', data.toString());
				}
			});


//			require('update-electron-app')({
//				repo: 'letheanVPN/app',
//				updateInterval: '1 hour'
//			});
		}
	);

	/**
	 * Quit when all windows are closed.
	 *
	 * Special handling for OS X, since it is common for applications and their menu bar
	 * to stay active until the user quits explicitly with Cmd + Q
	 */
	app.on('window-all-closed', () => {
		if (process.platform !== 'darwin') {
			app.quit();

		}
	});

	app.on('will-quit', () => {
		lthn.kill('SIGKILL');
	});
	/**
	 * On OS X it's common to re-create a window in the app when the
	 * dock icon is clicked and there are no other windows open.
	 */
	app.on('activate', () => {
		if (win === null) {
			createWindow();
		}
	});

	/**
	 * bypass ssl validation for lthn https interface,
	 */
	app.on('certificate-error', (event, webContents, url, error, certificate, callback) => {
		if (url.match('https://localhost:36911/')) {
			event.preventDefault();
			callback(true);
		}
	});


} catch
	(e) {
	// Catch Error
	throw e;
}
