.PHONY: all docs setup help app build docker dev run-daemon linux windows macos install-linux release-windows release-linux
.PHONY: release-macos clean

all: help

clean: ## Clean repo
	rm -r node_modules && rm -r app/node_modules

install-linux: ## Installs nodejs v16 and deno
	curl -fsSL https://deno.land/x/install/install.sh | sh

install-macos: ## Installs deno macos
	curl -fsSL https://deno.land/x/install/install.sh | sh

install-windows: ## installs deno on windows
	iwr https://deno.land/x/install/install.ps1 -useb | iex

docs: ## Documentation website, placed in ./public
	npm run compodoc && npm run compodoc:serve

dev: ## Start Development Server
	npm run dev

app: linux windows macos ## Build production build, all flavours
	npm run app:release

linux: ## Linux Test build
	npm run app:build:linux -- --publish=always

windows: ## Windows Test Build
	npm run app:build:windows  -- --publish=always

macos: ## MacOS Test Build
	npm run app:build:macos -- --publish=always

setup: ## installs packages
	npm install

define ANNOUNCE_BODY

  Welcome to LetheanVPN, Connecting Communities Since 2017

  Output Generated: $(shell date)

  Website: https://lt.hn
  Email:   mailto://hello@lt.hn

  COMMUNITY SUPPORT

  Telegram: https://telegram.lt.hn   KeyBase: https://keybase.lt.hn
  Hive:     https://peakd.com/@dvpn  Join: https://peakd.com/register?ref=dvpn
  Twitter: https://twitter.com/LetheanVPN

endef
export ANNOUNCE_BODY
help:
	@echo "$$ANNOUNCE_BODY"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36mmake %-30s\033[0m %s\n", $$1, $$2}'


