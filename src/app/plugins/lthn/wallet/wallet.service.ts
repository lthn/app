import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class WalletService {

	constructor(private http: HttpClient) {
	}

	startWallet() {
		const options = {
			headers: new HttpHeaders({
				'Content-Type': 'application/x-www-form-urlencoded'
			}),
			responseType: 'text' as 'json'
		};

		const request = {rpcBindPort: '36910'};

		return this.http.post<any>(`https://localhost:36911/daemon/wallet/rpc`, request, options).toPromise().then((dat) => console.log(dat));
	}
}
