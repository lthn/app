import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class BlockchainService {

	constructor(private http: HttpClient) {
	}

	startDaemon(): Observable<any> {
		const options = {
			headers: new HttpHeaders({
				'Content-Type': 'application/x-www-form-urlencoded'
			}),
			responseType: 'text' as 'json'
		};
		return this.http.post<any>(`https://localhost:36911/daemon/chain/start`, {}, options);

	}

	/**
	 * Export chain to a raw format
	 *
	 * @returns {Observable<any>}
	 */
	exportChain(): Observable<any> {
		const options = {
			headers: new HttpHeaders({
				'Content-Type': 'application/x-www-form-urlencoded'
			}),
			responseType: 'text' as 'json'
		};
		return this.http.post<any>(`https://localhost:36911/daemon/chain/export`, {}, options);

	}

	/**
	 * Import raw chain data file
	 *
	 * @returns {Observable<any>}
	 */
	importChain(): Observable<any> {
		const options = {
			headers: new HttpHeaders({
				'Content-Type': 'application/x-www-form-urlencoded'
			}),
			responseType: 'text' as 'json'
		};
		return this.http.post<any>(`https://localhost:36911/daemon/chain/import`, {}, options);

	}

	/**
	 * Download Lethean binaries to the users home dir
	 * ~/Lethean/cli
	 *
	 * @returns {Promise<void>}
	 */
	downloadCLI() {
		const options = {
			headers: new HttpHeaders({
				'Content-Type': 'application/x-www-form-urlencoded'
			}),
			responseType: 'text' as 'json'
		};
		return this.http.post<any>(`https://localhost:36911/update/cli`, {}, options).toPromise().then((dat) => console.log(dat));

	}
}
