import {createFeatureSelector, createSelector} from '@ngrx/store';
import {UsersState} from '@core/modules/user/data/user/state';

export const selectUserState = createFeatureSelector<UsersState>('users');

export const selectUserByID = (id: string) => createSelector(selectUserState, state => {
  if (state && state[id]) {
    return state[id];
  }
  return null;
});

