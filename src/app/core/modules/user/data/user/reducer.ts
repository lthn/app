import {Action, createReducer} from '@ngrx/store';
import {UsersState} from '@core/modules/user/data/user/state';


export const initialState: UsersState = {
	users: {}
};

const userReducer = createReducer(
	initialState
);

export function reducer(state: UsersState, action: Action) {
	return userReducer(state, action);
}
