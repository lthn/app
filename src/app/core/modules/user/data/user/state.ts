import {UserOptions} from '@core/modules/user/data/user/interfaces/user.options';

export interface UsersState {
  users: { [id: string]: UserState };
}

export interface UserState {
  id: string;
  username: string;
  options: UserOptions;
  meta: UserMeta;
}

export interface UserMeta {
  decryptionKey: string;
}
