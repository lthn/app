import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ChartDirective} from '@core/modules/chart/chart.directive';
import {ChartService} from '@core/modules/chart/chart.service';


@NgModule({
	declarations: [
		ChartDirective
	],
	imports: [
		CommonModule
	],
	providers: [ChartService],
	exports: [
		ChartDirective
	]
})
export class ChartModule {
}
