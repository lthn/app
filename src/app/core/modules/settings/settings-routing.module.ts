import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';
import {AuthGuard} from '@app/core/auth/route.guard';

const routes: Routes = [
	{
		path: 'settings',
		canActivate: [AuthGuard],
		loadChildren: () => import('@core/modules/settings/settings.module').then(m => m.SettingsModule)
	}
];

@NgModule({
	declarations: [],
	imports: [
		CommonModule,
		RouterModule.forChild(routes)
	],
	providers: [AuthGuard],
	exports: [RouterModule]
})
export class SettingsRoutingModule {
}
