import {TestBed} from '@angular/core/testing';

import {FileSystemWebService} from '@service/filesystem/file-system-web.service';

describe('FileSystemWebService', () => {
  let service: FileSystemWebService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FileSystemWebService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
