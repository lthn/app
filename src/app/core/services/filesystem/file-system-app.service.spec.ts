import {TestBed} from '@angular/core/testing';

import {FileSystemAppService} from '@service/filesystem/file-system-app.service';

describe('FileSystemAppService', () => {
  let service: FileSystemAppService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FileSystemAppService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
