import {Injectable} from '@angular/core';
import {FileSystemInterface} from '@core/interfaces/file-system.interface';
import {AppService} from '@app/app.service';
import * as fs from 'fs';


@Injectable({
	providedIn: 'root'
})
export class FileSystemAppService implements FileSystemInterface {

	private fs;
	private appPath;

	constructor(private appService: AppService) {
		if (window.require !== undefined) {
			this.fs = window.require('fs');
			this.appPath = window.require('path');
		} else {
			this.fs = this.appService.fs;
		}

	}

	path(type, dirname) {
		const {app} = window.require('@electron/remote');
		return this.appPath.join(app.getPath(type), dirname);
	}

	list(type, dirname) {
		return this.fs.readdirSync(this.path(type, dirname));
	}

	mkdir(type, dirname) {
		return this.fs.mkdirSync(this.path(type, dirname));
	}

	read(type, filename) {
		return this.fs.readFileSync(this.path(type, filename)).toString();
	}

	write(type, filename, data) {
		return this.fs.writeFileSync(this.path(type, filename), data);
	}

	exists(type, pathname): boolean {
		return this.fs.existsSync(this.path(type, pathname));
	}
}
