import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {AppModule} from './app/app.module';
import {APP_CONFIG} from '@env/environment';

if (APP_CONFIG.production) {
	enableProdMode();
}

document.addEventListener('DOMContentLoaded', () => {
	platformBrowserDynamic()
		.bootstrapModule(AppModule, {
			preserveWhitespaces: false
		})
		.catch(err => console.error(err));
});
