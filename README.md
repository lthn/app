# Lethean

## Build Requirements

NodeJS 16 & Angular 12 for GUI.

Deno for CLI installed to homeDir

## Project Folders

- `[src/* || app/*]`  GUI Application - NodeJS LTS (16 atm)
- `builders/cli/*` Client/server Exit-node - Deno (NodeJs replacement)

## Linux Ubuntu

```
make install-linux
make linux
```

## Windows

Linux Host

```
make install-linux
make windows
```

Windows Host Powershell - Install deno compiler

```shell
iwr https://deno.land/x/install/install.ps1 -useb | iex
```

Project checkout

```shell
npm install
npm app:build:windows

```

## MacOS

Install deno compiler

```shell
curl -fsSL https://deno.land/x/install/install.sh | sh

```

Project checkout

```shell
npm install
npm app:build:macos

```
